from Crypto.Cipher import AES
from Crypto.Util.Padding import pad

key = b'Sixteen byte key'
data = b'padoracleattack'
cipher = AES.new(key, AES.MODE_CBC)
ct = cipher.encrypt(pad(data, AES.block_size))
with open('../binFiles/ciphertext.bin', 'wb') as f:
    f.write(cipher.iv + ct)

cipher = AES.new(key, AES.MODE_CBC)
pt = cipher.decrypt(ct)
with open('../binFiles/plaintext.bin', 'wb') as f:
    f.write(pt)

# ciphertext[:16]
# for i in range(0, 16):
#     new_iv.append(ct[i]^pt[i])
