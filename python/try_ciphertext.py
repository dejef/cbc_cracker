#!/usr/bin/python3
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad

with open('../binFiles/attempt_ciphertext.bin', 'rb') as f:
    ciphertext = f.read()

# would have had to do a key exchange
key = b'Sixteen byte key'
cipher = AES.new(key, AES.MODE_CBC)
plaintext = cipher.decrypt(ciphertext)
plaintext = plaintext[16:]

# write this to a file and run haskell on it to see if it's legit
with open('../binFiles/attempt_plaintext.bin', 'wb') as f:
    f.write(plaintext)
