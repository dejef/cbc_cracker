import qualified Data.ByteString.Lazy as BL -- fundamental to the program
import Data.Char (chr)                      -- printing solution 
import Data.Binary.Get                      -- getting file contents
import Data.Bits                            -- `xor` operation
import Data.List                            -- many list functions
import Data.Word                            -- the Word8 type
import System.Process                       -- outsourcing to Python
import Control.Conditional                  -- Mondad ifM

                                                      
main = do intercepted_ct <- getWordsIO "../binFiles/ciphertext.bin"
          oAF intercepted_ct (0::Word8)
      
oAF  :: [Word8] -> Word8 -> IO ()
oAF ws i = do let attempt_ct = case i of
                                 0 -> do let (t:ts) = ws
                                         (88::Word8):ts -- no idea why 88 -> 19
                                 _ -> do let (x,_:ys) = Prelude.splitAt (fromIntegral(i)) ws
                                         x ++ (17::Word8) : ys
              createAttemptCiphertext (BL.pack attempt_ct)
              ifM isValidCiphertext 
                  (oAF ws (i + 1)) 
                  (oAS ws i [])
 
-- inject a character into the penultimate block to have an effect on the last one
oAS :: [Word8] -> Word8 -> [Word8] -> IO ()
oAS ws i pt = do let p = (16 - i)::Word8 
                 let g = 1::Word8
                 let t_posn = (((length ws) - 16) - 1) - ((16 - fromIntegral(i)) - 1)
                 let t = ws !! t_posn 
                 oAT ws t t_posn g p pt

-- repeatedly try and find the plaintext character
oAT :: [Word8] -> Word8 -> Int -> Word8 -> Word8 -> [Word8] -> IO ()
oAT ws t t_posn g p pt = do let f_part = fst (splitAt t_posn ws) 
                            let new_ws = (f_part ++ g:[]) ++ tail (snd (splitAt t_posn ws))
                            createAttemptCiphertext (BL.pack new_ws)
                            ifM isValidCiphertext 
                                (do let np = p + 1
                                    let v_new_ws = updateForNextPad new_ws (fromIntegral(t_posn)) (np)
                                    case p of 
                                      16 -> print ( map (chr . fromEnum) (reverse pt))
                                      _  -> oAS v_new_ws (fromIntegral(t_posn - 1)) 
                                                         (pt ++ ((t `xor` g `xor` p):[])))
                                (oAT new_ws t t_posn (g + 1) p pt)


-- recursively change all of the previously found characters in expectation of the new find
updateForNextPad     :: [Word8] -> Word8 -> Word8 -> [Word8]
updateForNextPad ws t_posn np   = if t_posn == 16 
                                    then ws 
                                  else updateForNextPad new_ws (fromIntegral(t_posn + 1)) np
                                  where new_ws = upsert ws (fromIntegral(t_posn)) np 

upsert :: [Word8] -> Word8 -> Word8 -> [Word8]
upsert ws posn p   = x ++ val : ys
                     where val = t `xor` (p - 1) `xor` p
                           (x,t:ys) = Prelude.splitAt (fromIntegral(posn)) ws

createAttemptCiphertext  :: BL.ByteString -> IO ()
createAttemptCiphertext b = do let fp = "../binFiles/attempt_ciphertext.bin"::FilePath 
                               BL.writeFile fp b

-- this is a server function, to the attacker all it returns is a True or False indicating 
-- the validity of the ciphertext.
isValidCiphertext :: IO Bool
isValidCiphertext  = do tryCiphertext
                        attempt_pt <- getWordsIO "../binFiles/attempt_plaintext.bin" 
                        return (isValidPadding attempt_pt)

tryCiphertext :: IO ()
tryCiphertext  = callProcess "../python/try_ciphertext.py" []

-- returns True if the padding is valid
isValidPadding   :: [Word8] -> Bool 
isValidPadding bs = if isPadByte (last bs) 
                      then all (n == )(drop (16 - x) bs) 
                    else False
                    where x = fromIntegral(last bs)     
                          n = last bs                  

isPadByte  :: Word8 -> Bool                          
isPadByte b = 1 <= b && b <= 16                     

getWordsIO   :: FilePath -> IO [Word8]   
getWordsIO fp = do input <- BL.readFile fp                      
                   return(runGet getWords input)
                                              
getWords :: Get [Word8]                      
getWords  = do empty <- isEmpty                                           
               if empty                    
                 then return []           
               else do w <- getWord8     
                       ws <- getWords   
                       return (w:ws)   
