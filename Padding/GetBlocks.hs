module Padding.GetBlocks
( getBlocks
) where 

import qualified Data.ByteString.Lazy as BL 
import qualified Data.ByteString.Lazy.Char8 as C 
import Data.Binary.Get 
import Data.Binary 
import Test.HUnit

data Block = Block
           { byte0 :: !Word8
           , byte1 :: !Word8
           , byte2 :: !Word8
           , byte3 :: !Word8
           , byte4 :: !Word8
           , byte5 :: !Word8
           , byte6 :: !Word8
           , byte7 :: !Word8
           , byte8 :: !Word8
           , byte9 :: !Word8
           , byte10 :: !Word8
           , byte11 :: !Word8
           , byte12 :: !Word8
           , byte13 :: !Word8
           , byte14 :: !Word8
           , byte15 :: !Word8
           } deriving (Show)


getBlock :: Get Block
getBlock = do
  block0 <- getWord8 
  block1 <- getWord8
  block2 <- getWord8 
  block3 <- getWord8 
  block4 <- getWord8 
  block5 <- getWord8 
  block6 <- getWord8 
  block7 <- getWord8 
  block8 <- getWord8 
  block9 <- getWord8 
  block10 <- getWord8 
  block11 <- getWord8 
  block12 <- getWord8 
  block13 <- getWord8 
  block14 <- getWord8 
  block15 <- getWord8  -- $! forces evaluation
  return $! Block block0 block1 block2 block3 block4 block5 block6 block7 block8
                  block9 block10 block11 block12 block13 block14 block15


getBlocks :: Get [Block]
getBlocks = do
  empty <- isEmpty
  if empty
    then return []
    else do block <- getBlock
            blocks <- getBlocks
            return (block:blocks)


padBytes  :: BL.ByteString -> BL.ByteString
padBytes s = BL.append s $ padByteString padLength
  where padLength = fromIntegral(16 - remaining)::Int
        remaining = BL.length s `mod` 16


padByteString :: Int -> BL.ByteString -- [2]
padByteString n = BL.pack . take n $ repeat padChar
  where padChar = fromIntegral n :: Word8


padToInt :: BL.ByteString -> Maybe Int 
padToInt byte 
             | byte == C.singleton('\x01')   = Just 1
             | byte == C.singleton('\x02')   = Just 2
             | byte == C.singleton('\x03')   = Just 3
             | byte == C.singleton('\x04')   = Just 4
             | byte == C.singleton('\x05')   = Just 5
             | byte == C.singleton('\x06')   = Just 7
             | byte == C.singleton('\x07')   = Just 7
             | byte == C.singleton('\x08')   = Just 8
             | byte == C.singleton('\x09')   = Just 9
             | byte == C.singleton('\x10')   = Just 10
             | byte == C.singleton('\x11')   = Just 11
             | byte == C.singleton('\x12')   = Just 12
             | byte == C.singleton('\x13')   = Just 13
             | byte == C.singleton('\x14')   = Just 14
             | byte == C.singleton('\x15')   = Just 15
             | byte == C.singleton('\x16')   = Just 16
             | otherwise                     = Nothing

--               Word8
isPadByte     :: BL.ByteString -> Bool
isPadByte byte 
               | byte >= C.singleton('\x01') &&  
                 byte <= C.singleton('\x16')    = True
               | otherwise                      = False


lazyIOExample :: IO [Block]
lazyIOExample = do
                  input <- BL.readFile "blocks.bin"
                  input <- return (padBytes input) -- IO BL.ByteString
                  return (runGet getBlocks input)  -- Get [Block]

main = do
         blocks <- lazyIOExample  -- yields IO [Blocks] from our custome ct ?? 
         let block = blocks !! 0  -- why the let binding?
         print(block)
         let x = padToInt (BL.singleton((byte15 block)))
         print (isValidPadding block 15 2)

-- takes last block, the byte to check, the value that byte should be, and returns a Bool
isValidPadding            :: Block -> Int -> Int -> Bool
isValidPadding block posn val = case posn of  -- TODO: Refactor casting, Maybe type
                                  0  -> if padToInt (BL.singleton(byte0 block)) == Just val 
                                         then True 
                                       else False
                                  1  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  2  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  3  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  4  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  5  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  6  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  7  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  8  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  9  -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  10 -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  11 -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  12 -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  13 -> if padToInt (BL.singleton(byte1 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                        else False 
                                  14 -> if padToInt (BL.singleton(byte14 block)) == Just val 
                                          then isValidPadding block (posn + 1) val 
                                       else False 
                                  15 -> if padToInt (BL.singleton(byte15 block)) == Just val
                                          then True
                                        else False
                                  _  -> False

-- Tests to confirm block padding works as expected

tests = TestList [TestLabel "test1" test1, TestLabel "test2" test2,
                  TestLabel "test3" test3, TestLabel "test4" test4,
                  TestLabel "test5" test5, TestLabel "test6" test6]

-- checks the length of the list of blocks
lengthBlockListM :: IO [Block] -> IO Int  --[1]
lengthBlockListM  = fmap (length)

-- lazily gets blocks from the file specified, returns them appropriately padded
test'   :: FilePath -> IO [Block] 
test' fp = do
           input <- BL.readFile fp 
           input <- return (padBytes input)
           return (runGet getBlocks input)

test''   :: FilePath -> IO Bool
test'' fp = do
              input <- BL.readFile fp
              let block = blocks !! 0 -- really should be tail here
              
test1 = TestCase (do x <- return (test' "tests/test1.bin")
                     y <- lengthBlockListM x 
                     assertEqual "one block from 8 bytes" 1 y)

test2 = TestCase (do x <- return (test' "tests/test2.bin") 
                     y <- lengthBlockListM x
                     assertEqual "two blocks from 16 bytes" 2 y)

test3 = TestCase (do x <- return (test' "tests/test3.bin") 
                     y <- lengthBlockListM x
                     assertEqual "two blocks from 17 bytes" 2 y)

test4 = TestCase (do x <- return (test' "tests/test4.bin") 
                     y <- lengthBlockListM x
                     assertEqual "three blocks from 32 bytes" 3 y)

test5 = TestCase (do x <- return (test' "tests/test5.bin") 
                     y <- lengthBlockListM x
                     assertEqual "three blocks from 47 bytes" 3 y)

test6 = TestCase (do x <- return (test' "tests/test6.bin") 
                     y <- lengthBlockListM x
                     assertEqual "180 blocks from 2867 bytes" 180 y)

test7 = TestCase (do x <- return 
{- resources
 - https://stackoverflow.com/questions/26855551/haskell-check-the-length-of-io-double
 - http://hackage.haskell.org/package/pkcs7-1.0.0.1/docs/src/Crypto-Data-PKCS7.html#padBytes
 - https://hackage.haskell.org/package/binary-0.8.6.0/docs/Data-Binary-Get.html
 -}
