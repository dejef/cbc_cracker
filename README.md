# Functional Project
## Jeffrey De La Mare
### CS 557 Functional
### 03/21/2019

#### Introduction
This paper details the use of a functional language applied to a cryptographic attack. The attack uses a padding oracle from which very little information is given to an attacker. Yet they can go on to learn more about a captured encoded message if they have a mechanism which provides negative feedback. Admittedly, this problem doesn't lend itself to Haskell's flexibility in wielding user defined types - in fact the only type we really care about is that of a bytestring. Even still we'll find that it's possible to create an attack which may more commonly be seen in an imparitive language. One thing to note here at the very start- this is not a techinical document. In fact, it's very far from it. In order to write a sufficient amount I've decided to keep the tone somewhat loose (still factual) and elaborate on areas that may not even be included in the source code. These likely include my short-comings, unnecessary detours, struggles in understanding, and general confusion that arose while creating the program. However it will not contain any tone of frustration, because I actually did have quite a bit of fun writing this code.  

#### Program structure
Initially, this program was going to exist in a couple of directories *neatly* named Oracle Padding, and perhaps Util. Unfortunately, this led to quite a bit of confusion on both my part and that of the compiler. Evidently, the way that I wanted to use modules created a "cyclic" dependency error. Unfortunately I was unable to resolve these as most examples online involved specifying a particular user datatype as part of the module. That (to my knowledge) didn't seem to be the case with my program, as it was actually conflict of my exporting various functions. I'm certain of this because I never got around to using the keyword `data`. The end result has the code in a single file, which fits its script-like nature. I seem to have trouble building elegant programs, but I imagine it just takes practice. 

What has been mentioned previously only applies to the Haskell code, but it should be noted that I used (eek) Python to assist in a domain that it prevails in. I was unable to find a reliable off-the-shelf cryptographic library for use with in a Haskell program. The way that I circumvent this is by utilizing `System.Process` to call the Python programs and read the data from a file. This lends the code to working almost entirely in the `IO` monad, which admittedly makes print debugging feasible. The Python files have their own directory `./python` and the files that are read and written to live in `./binFiles` directory. All of these files are written out as with the `byte` type, so it's easy to examine them with the `xxd` program.

#### Many different paths...
Being that Haskell is a strongly typed functional language, at compile time we must bow to the type checker. I've already revealed that the program utilizes bytestrings throughout, more specifically `Data.ByteString.Lazy`. When this program was but a wee 20 lines it was assumed that a data type of `Block` would be used. It was defined as a record type, which handily enough includes an accessor function for each member of the type. A `Block` was to represent an AES 16 byte block and would thus have 16 members labeled `byte0` through `byte16`. This proved to be quite unwieldy, and actually resulted in a few uses where I was unable to leverage the language to do what I thought was needed. I heeded some (very) good advice to take a look at the list type again. Useful as this was I'm still curious if an `Array` would have been more flexible in the long run. In the end, I had three possible paths and decided that the most reasonable way of programming this attack was to use the list type.

#### The problem with CBC Mode
Bear with me, I'm going to need to explain this crypto problem for just a minute. This was probably a foolish undertaking in that it's a complicated attack and difficult to implement in a new language. Here we go.

The padding oracle is just a function that can take a string of bytes and determine whether or not they're correctly padded. If an attacker were to repeatedly send gibberish to the oracle with a length not evenly divisible by 16 they would be constantly rejected. The grounds for this rejection are due to the decryption of an expected ciphertext via the block cipher AES. AES using CBC mode only likes values in chunks of 16 bytes, so when we send something to the oracle that doesn't neatly align with that we're given an expected rejection. This may lead one to think that anything divisible by 16 bytes is a valid candidate. But that is not the case, as their is one more necessary qualification to be accepted by the oracle and have a message decrpyted. One more thing- CBC Mode is one of many different types of encrpytion used in conjunction with AES. This attack affects won't work on modes where the ciphertext doesn't directly influence the next blocks decryption. Anyways, back to padding.

#### PKCS#7 Padding
This is a padding method most commonly seen in block ciphers. In fact, there is a succint library that utilizes ByteStrings[1]. This is my first major undertaking using Haskell in an unconstrained environment. That being said, I leaned heavily (hopefully not too much) on resources found online and the good advice of my Professor. The first place I'd like to point out where this shows is in the `padBytes` function. 


```haskell
import qualified Data.ByteString.Lazy as BL 
import Data.Word as W

padBytes  :: BL.ByteString -> BL.ByteString
padBytes s = BL.append s $ padByteString padLength
  where padLength = fromIntegral(16 - remaining)::Int
        remaining = BL.length s `mod` 16

padByteString :: Int -> BL.ByteString
padByteString n = BL.pack . Prelude.take n $ Prelude.repeat padChar
  where padChar = fromIntegral n :: Word8

padBytes (BL.pack ([1::Word8, 2::Word8, 3::Word8]))
```


    "\SOH\STX\ETX\r\r\r\r\r\r\r\r\r\r\r\r\r"


Now I technically never use this function in my code, the only one that I needed is a variation that confirms if a `[Word8]` has valid padding. I'll include the code for completeness, but I don't think that it illustrates what is happening with a padding function as well as `padBytes` does. Here's what's going on in the functions. The difference in AES block size and the length of the `ByteString` provided is calculated. That length is held in the `padLength` variable in the `where` clause of the uppermost function. We'll want to append a list of the appropriate number of repeated characters to ensure our block is correctly padded. The bytes that are chosen for padding are the same as the length of the padding. So one byte of padding is `\x01` and sixteen is `\x10`. Here we can see bytes indicating the "start of heading", "start of text", and "end of text" are padded out with a `\x0D` carriage return. Cool! So it looks like that works. Not too surprising since it's pretty much the library code linked at the bottom. Here is the version that was construed with much help. Note that the last four elements of the only argument to the second call of `isValidPadding` are all equal. Furthermore they reflect the length of missing bytes from previous list of only 12 elements in regards to AES block size.   


```haskell
isValidPadding   :: [Word8] -> Bool
isValidPadding bs = if isPadByte (last bs)
                      then all (n == )(drop (16 - x) bs)
                    else False
                    where x = fromIntegral(last bs)
                          n = last bs
                          
isPadByte  :: Word8 -> Bool
isPadByte b = 1 <= b && b <= 16 

isValidPadding ([1::Word8, 2::Word8, 3::Word8, 4::Word8, 5::Word8, 6::Word8, 7::Word8, 8::Word8,
 9::Word8, 10::Word8, 11::Word8, 12::Word8])

isValidPadding ([1::Word8, 2::Word8, 3::Word8, 4::Word8, 5::Word8, 6::Word8, 7::Word8, 8::Word8,
 9::Word8, 10::Word8, 11::Word8, 12::Word8, 4::Word8, 4::Word8, 4::Word8, 4::Word8])
```


    False



    True


#### Getting Data
Alright I've padded that explanation of PCKS#7 padding out about as far as need be, what else is interesting about this program? Well hold on to your seat, because we're going into the `IO` monad. Why you may ask? Why taint such a great language with side-effects... Well it's because I need to outsource some labor to Python, also it was doomed under my fingertips anyways. Now we've got a type signature just to prove it. 

The first place to note where this happens is in `main`, the driver of the program. A README.md file is included to indicate how the program should be run, simply fire up GHCi and type "main". So `main` only includes two lines of code in a `do` block. The first indicates that it should be some ciphertext from the `./binFiles` directory. Come to think of it, that might be the one necessary step before running the program. We'll need to have "captured" some ciphertext to decrypt. To pull that ciphertext into the program I've used the `readFile` command from the `Data.ByteString.Lazy` library. It's very important that I indicate here that the pattern for this code is largely borrowed from the an example on the webpage for `Data.Binary.Get`[2] This example showed me how I can easily grab the contents of a file as bytestrings, pass those to a helper from which the `Get` Monad can build up various states. The `Get` monad works in a lazy fashion, so nothing from the file is read until commanded to by a function like `getWord8`. `Data.ByteString.Lazy`'s `readFile` shouldn't have read anything until the call to `getWord8`, at which point it'll lazily grab 32kb from the file. Which is more than adequate for our needs (we're only getting 32 bytes).


```haskell
getWordsIO   :: FilePath -> IO [Word8]
getWordsIO fp = do 
                  input <- BL.readFile fp
                  return(runGet getWords input)

getWords :: Get [Word8]
getWords  = do
              empty <- isEmpty
              if empty
                then return []
              else do w <- getWord8
                      ws <- getWords
                      return (w:ws)
```

Going into slightly more detail on the code above, we can see that `getWordsIO` is sort of the driver that has `getWords` do the heavy lifting. Sure, we read in the `input` once from the driver, but it's the helper that recurses through each call to itself for however many 8 bit chunks exist in the file being read from. Because a `do` block maintains the same monad throughout, we can be sure that `isEmpty` is a function brought into scope from `import Data.Binary.Get`. This is also where the calls to `getWord8` and `runGet` come from. `runGet` is probably the most interesting function as it executes the `Get` monad. This is an excellent example of a monad in use because the word "get" is a very simple verb and highlights the action that the procedure is doing. It's "getting" things from the `input` file by taking a type `Get [Word8]` and a bytestring from which to get those values. The first parameter to the `runGet` decoder is somewhat confusing at first, but it's really just allowed to be a function in the same library. More specifically anything that shares the same type signature (somewhat obvious) and is described as being a decoder. So `runGet` yields an `a` that is the non-monadic type of its decoder. At the end of the `do` block we wrap it in a `return` to stay consistent with our type signature indicating a return value of type `IO [Word8]`.

It might be useful to stop here an mention why we're only getting 32 bytes from the input file. I'm going to include a short picture on CBC Mode decryption that might help clarify why this is the case. As a preface to the image, it's best to keep the ciphertext short. A well made cracking program for this attack should be able to run on many blocks of captured ciphertext. However, this code takes quite some time and memory to run, likely due to many recursive calls.  

<img src="../imgs/cbc-decrypt.png">

It may not be immediately obvious, but the reason this attack is possible is apart of the image above. The process captured by the red box shows that it's possible to affect the output ciphertext by changing a byte in the previous block of ciphertext. There is a one to one correspondence of this due to the nature of `xor`. If one thing in the ciphertext is changed, evidence of that change will exist in the plaintext of the decrypted block. You may be wondering, but what if there is only one block of encrypted ciphertext? Wouldn't we be missing the crucial previous block from which we can toggle plaintext characters? Not to worry, on the left most side of the picture you'll see an intialization vector (IV) that is usually a random nonce. That IV is to be `xor`d witht the first block of ciphertext. 

This actually highlights an important step in thinking. We don't really care what's in the previous block of ciphertext. It's just a means of changing the output that is plaintext. So this is novel, but it still needs to be bridged with other concepts to become a flushed out attack. For now, we're doing with CBC is changing some bytes in the ciphertext of the previous block. In practice, we're not allowed to see the plaintext (surprising, no?) because it exists on another machine the attacker does not control. In short, this is thought of as an "on path" atttack. We can grab the data, change it, and send it off to wreak havoc. Or in this case, learn something we didn't previously know.

#### Is it valid?
Where's the functional programming?! This paper is invalid. That's what a server may say if it judged papers and returned simple output in the form of "valid" or "not valid". So what an attacker may do is modify the paper a little bit, send it back, and see if it's happy. Unfortunately we don't have the luxury of repeated submissions, but one can dream. We can however, simulate a server telling us whether or not a submitted ciphertext is valid. This code was already shown above, it's the `isValidPadding` function. The keyword here is "padding". The server wants to confirm that the ciphertext we've passed is correctly padded (i.e. four bytes of padding means the block has four repeating `\x04` bytes). The attack leverages this information provided by the server to incrementally change the ciphertext. Why this works is a bit of a stumper, but has to do with the properties of the `xor` operation. This isn't the first step in the attack, but I'll unveil it here as to not keep the reader in the dark. 

We know we can manipulate the ciphertext, it's the piece of captured information from which we can leverage this attack. We know that manipulating the ciphertext has a direct effect in the corresponding byte of the plaintext. We also know that the server cares about padding. From this information we can deduce a plaintext character by trying all possible plaintext characters from decimal 0-255.

I found this difficult to grasp, and after much "hacking" away at the problem, I think there is an easiest way to present the series of values that must be ascertained. We'll be working entirely with the `xor` operator which is both commutative and associative. We're allowed to build equations and then solve for various unknowns provided that there is only one. Actually, we have two unknowns at the earliest point in the problem, but we can constrain our guess to be within 0-255, making this problem quite tractable for a computer.

Four values in question are:
* __T__ the target value from the previous block of ciphertext
* __G__ the guess from 0-255. I'll elaborate on what we're guess for in a bit.
* __Pt__ the plaintext that we wish to decode
* __Pd__ the padding character that the server evaluates as being correct or incorrect.

I find the easiest way to setup the equation is
```
Pd = T ^ G ^ Pt
```
Interestingly, I'm not solving for the plaintext. I find it quite confusing when trying to figure out why some character is equal to a jumble of others, and relate that to the server's signal of correct or incorrect. So instead we solve with respect to the padding character, and we know its desired value based on which plaintext byte we're looking to solve. If you take a peak at the python script, you'll find that the (correctly padded) encoded plaintext is "paddingoracleattack\x01". So when looking for the 'k' character we'll need to find what ciphertext character turns that '\x01' into a '\x02'. Overwrite the right most ciphertext block so the encoded (and invalid) plaintext would decrypt to "paddingoracleattack\x02". Then solve for the following equation, likely trying many versions of `G` before finding a solution with valid padding. Let's assume that the target value is a smiley face even though that takes two bytes to represent.
```
\x02 = :) ^ [0-255] ^ Pt
```
Importantly, the server is solving something like this equation, but since it knows the plaintext it only cares to see if the padding is correct. Anyways, this problem is able to be solved, and just requires a few guesses and extraction of certain list elements.

#### Back to your scheduled program

The way the attack works has been largely glossed over, but hopefully this handwaving in combination with some code will clarify the technique used to break CBC mode encryption.

I'm going to speak mostly on the way the code is structured for now, and loosely tie that into the algorithm for cracking. The only function of interest is `main`, so when loading this code up to be run in GHCi, simply type the function name and the crack will begin. The first step in the `do` block of `main` is extract the intercepted ciphertext from a file in `./binFiles`. Once we've got our `[Word8]`, we seed that with a `0` and feed it two one of three stages in the padding oracle attack. The code for this stage is below, named `oAF` as an initialism for "oracle attack first".


```haskell
main = do intercepted_ct <- getWordsIO "../binFiles/ciphertext.bin"
          oracleAttackFirst intercepted_ct (0::Word8)
```

#### Stage one

The first stage of this attack is more simple than the last two, and only requires as much as 15 iterations to glean the necessary information. We have 32 bytes of ciphertext in question, how many of the first 16 do we have to alter till we're provided an invalid padding error? If the entire block is '\x10', as is the case when the plaintext's length mod the block size is equal to zero, then we move on to the next ciphertext block. However, if it is anything but '\x10', we'll have to retry fudging the ciphertext all the way from the start of that block to the last element. 


```haskell
oAF :: [Word8] -> Word8 -> IO ()
oAF ws i = do let attempt_ct = case i of
                                 0 -> do let (t:ts) = ws
                                         (17::Word8):ts
                                 _ -> do let (x,_:ys) = Prelude.splitAt (fromIntegral(i)) ws
                                         x ++ (17::Word8) : ys
              createAttemptCiphertext (BL.pack attempt_ct)
              ifM isValidCiphertext 
                  (oAF ws (i + 1)) 
                  (oAS ws i [])
```

Refering to `main` we see that this code is seeded with the value `0` so that the first attempted ciphertext checks to see if the entire block is padding characters. Here is the first definitive issue in using Haskell, where values like a list are immutable. It makes sense to note again that everything is done in the `IO` which allowed for use of `do` notation and the creation of local variables by way of a `let` binding. In order to circumvent altering state I used `let` to create `attempt_ct` which is a newly altered version of the ciphertext taken in from `ciphertext.bin`. If this is the first time `oAF` has been called, then the second argument to the function will be `0` à la `main`. Any subsequent calls will see this value increased, landing in the catch-all of the `case` statement which chooses the value of `attempt_ct`. So the issue with immutablility is as follows: I'm unable to reuse the list, so I must craft a new one. There is no easy way to update an entry in the list, so it must be split at the appropriate index. Thankfully, this index is carried through the recursive calls to `oAF` in the `i` variable. When `i` is `0`, we can simple use the cons notation to dequeue the list and append it's tail to an invalid padding character. Here `17::Word8` is used to create the first invalid character of all 256 possible characters that can be represented with a single byte. 

The hackiness for this problem was actually sourced from a Stackoverflow article [4]. It's a clever way of splitting a list, dropping the element in question, and then rejoining the two lists with an altered head of the second list. The catch-all of the case statement is where this operation happens. We can take the index `i` which of type `Word8` and cast it up to an `Integral` from which an `Int` is an instance of. This casting is necessary because `splitAt` from the prelude requires an `Int` argument to index on, and unfortunately `Word8` would not cooperate. Further on in the description of the program we'll see other cases where we needed to bounce back and forth between various types. Although it's important to note that we never exceed the value of 255.

Ok ok! We've got the attempted ciphertext, now what? We'll we run a function `createAttemptCiphertext` and pass it a `ByteString` created by passing `[Word8]` to the function `pack`. I've imported the library `... as BL` so that we can shorten calls to the function with no need to specify the full location from whence it came. Here's `createAttemptCiphertext`:


```haskell
createAttemptCiphertext  :: BL.ByteString -> IO ()
createAttemptCiphertext b = do let fp = "../binFiles/attempt_ciphertext.bin"::FilePath
                               BL.writeFile fp b
```

It's a really simple function that takes a hardcoded path to to write out the bytes of provided as `b`. Nothing crazy is happening here, it's a cut and try write function that takes exactly the bytes provided and pipes them into a file named `attempt_ciphertext.bin`.

From here we call a slightly more interesting function `isValidCiphertext`.


```haskell
isValidCiphertext :: IO Bool
isValidCiphertext  = do tryCiphertext
                        attempt_pt <- getWordsIO "../binFiles/attempt_plaintext.bin"
                        return (isValidPadding attempt_pt)
                        
tryCiphertext :: IO ()
tryCiphertext  = callProcess "../python/try_ciphertext.py" []
```

The first action of `isValidCiphertext` is to call out to the Python script (included at the end of this document) to create a CBC cipher and attempt decryption with the provided ciphertext. The script writes to `attempt_plaintext.bin` which is read in via the same function as in `main`- `getWordsIO`. We check to see if that plaintext is correctly padded. Again we've simulated a server which tells an unknown attacker if some ciphertext is valid or not. This returns a `Bool` which we must wrap in the `IO` monad with the `return` function. This makes quite a bit of sense, we're not sure if the file exists, reading the file is itself an action, and there is a lot that can go wrong here. Thankfully this program is short enough that finding these weakspots didn't require a special handler. I will note however that the python script used to write to a directory `./output/` and this caused a great deal of confusion for the developer for quite some time.

Anyways, we can leap back to the most interesting function `oAF`. There is a special function `ifM` from the library `Control.Conditional` library. It was at this point that I wasn't sure if I was completely tainting this language with my error prone coding style, or if something is genuinely missing. It makes sense that an action has a result, and in the series of actions on within a `do` block we might want to see the result of said action. Well this involved a little thought and a lot of recklessness. I decided to go forward with including `ifM`, and thankfully it worked out in the end. I peaked at the documentation for this function but gained a majority of my understanding from this article [5]. The function takes a monadic `Bool` and evaluates if it is `True` or `False` with 
```
do b <- b
   if b
       -- branches

```
then branches and returns the second or first argument depending on how that boolean is evaluated. After peaking at the source code for `ifM` [6], it would have been simpler to implement my own function. I had already downloaded the "cond" library at this point and just decided to roll with it. So the control flow is identical to a standard conditional. If it's true the first argument is evaluated, if it's false the second. We can see that `isValidCiphertext` returns an `IO Bool`, so until we find the index of the first padding character we'll keep recursively calling `oAF`. Once we find some invalid padding, we know the index that the padding characters begin on. We can pass this the original ciphertext bytes `ws`, index at which the padding begins `i`, and an empty list to `oAS`. `oAS` is of course an initilism for "oracle attack second". 

#### Stage two

This stage is largely a helper for the third stage that does the actual cracking. There are a lot of key variables we're interested in, for instance the target in the ciphertext that we'd like to alter. Again, we'll progressively change this target until we're able to find the plaintext using the `xor` equation above. The target is denoted below as `t`. We'll need to know the target's value, and since we're working with a list we'll also have to know the position at which it lies with respect to the list's head. We can extract a value at that position with the `!!` operator which takes operations with respect to the length of the list. Calculating the position is a little bit more complicated than one might expect. The operation will only ever grab the second to last block of ciphertext, so the minimum length of the ciphertext is 32 bytes, but there isn't a theoretical upper bound that I'm aware of. Now might be a good time to slip in that fact that we'll never be able to decrypt the first block the ciphertext with this attack, sorry.

Anyways, it can be seen we're seeding the third stage of the attack with a preliminary guess of '1'. We've been passed the index from `oAF`, and we can find the padding character `p` we'd like to solve for.


```haskell
oAS :: [Word8] -> Word8 -> [Word8] -> IO ()
oAS ws i pt = do let p = (16 - i)::Word8
                 let g = 1::Word8
                 let t_posn = (((length ws) - 16) - 1) - ((16 - fromIntegral(i)) - 1)
                 let t = ws !! t_posn
                 oAT ws t t_posn g p pt
```

Below is a list of `Int` that each have a value equal to their corresponding position in the array. The purpose of `t_posn` is to find the target at a given position from the back of an array. We only ever care about the second to last block of the ciphertext when we're looking for a target. Another thing to note is that I've hardcoded the value for the block size as 16, when in reality this could change when using different vulnerable block cipher.


```haskell
import Numeric
ws = [0x0,0x1,0x2,0x3,0x4,0x5,0x6,0x7,0x8,0x9,0xa,0xb,0xc,0xd,0xe,0xf,0x10,0x11,
      0x12,0x13,0x14,0x15,0x16,0x17,0x18,0x19,0x1a,0x1b,0x1c,0x1d,0x1e,0x1f,0x20,
      0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x28,0x29,0x2a,0x2b,0x2c,0x2d,0x2e,0x2f]

t_posn :: [Int] -> Int -> Int
t_posn ws i = ((length ws) - 16 - 1) - (16 - i - 1)
and [ t_posn ws i == ws !! 16 + i | i <- [0..15] ]
showHex (t_posn ws 15) "" -- this is the right most character of the second to last blowck
```


    True



    "1f"


#### Stage three

This function does the brute of the work, and is recursively called as many as 254 times per cracked character. We know that the padding character is not allowed to a be '\x00', so the final call will be to check if guess should be '\xff'. This function resursively calls itself many times, but it also works towards a base case by recursively calling `oAS`. That being said, it seems accurate to refer to these two functions as mutually recursive. Note however, that stage one is completely separate from stage two and three. Although these stages are more closely coupled than the first, they are distinct and as such I have chosen to define them uniquely as different steps in the overall algorithm. 

The `oAT` function takes quite a few parameters, to the point that it may be worth refactoring in the future. The reason so many parameters are used is partially because state must constantly be updated, and those changes must be carried through the computations. Another reason why so many variables are needed has to do with the implemenation's use of the list type. The list may have hindered us earlier in updating values, but ultimately it has been a useful and simple data structure. This aids in trimming down the complexity of the program, but at a cost of needing to keep track of some important values. For one, which index do we care about? That is something that needs to be passed along as the program is ran, lest we pick an incorrect target and the operations begin to fail. When working at this level, with so many previous computations building atop the current one, errors can propagate in unbelievable ways. 

When writing this function I found it crucial to be certain that I was performing the correct calculation. I often had another window open running GHCi to confirm that computations had the desired outcome. It is easy to run into classic off by one errors when dealing with the list type. This can be seen most easily in `oAS`'s `t_posn` variable for which an example and tests were flushed out above. Again, another downside of the list type is its lack of instant access, which is quite desirable in a function that does this much list manipulation. We'll see again that there is a clunky line of code where an element needs to be updated. 


```haskell
oAT :: [Word8] -> Word8 -> Int -> Word8 -> Word8 -> [Word8] -> IO ()
oAT ws t t_posn g p pt = do let f_part = fst (splitAt t_posn ws)
                            let new_ws = (f_part ++ g:[]) ++ tail (snd (splitAt t_posn ws))
                            createAttemptCiphertext (BL.pack new_ws)
                            ifM isValidCiphertext
                                (do let np = p + 1
                                    let v_new_ws = updateForNextPad new_ws (fromIntegral(t_posn)) (np)
                                    case p of
                                      16 -> print ( map (chr . fromEnum) (reverse pt))
                                      _  -> oAS v_new_ws (fromIntegral(t_posn - 1))
                                                         (pt ++ ((t `xor` g `xor` p):[])))
                                (oAT new_ws t t_posn (g + 1) p pt)
```

There's a fair amount going on here, but what's key to note is that the incoming `ws` will have been updated twice if coming from mutually recursive call to `oAS`. It will have been updated only once if it's coming from within `oAT`. It is updating `ws` twice if the next plaintext character has been found. It's only updating once if we're in need of trying a different guess. Before digging into the function, let's gloss over how the cracking works once more. We'll go to a position in the list and attempt to crack that character. If we manage to succeed, great! Now we can add that plaintext to the list `pt` and move on to cracking the other character right? Almost right, but not quite. It's super important that we also update the other previously cracked characters of the ciphertext in expectation for a larger padding size. In other words, we've got to go back and change everything so that 
```
t `xor` (p - 1) `xor` p
``` 
equals the next padding byte when it is `xor`d against the output of the server's decrypt function. I will admit, this probably took the longest time to figure out. I had to scrape quite a few resources to and try lots of combinations till I could figure out exactly why this wasn't working as desired. Since I've already pushed off talking more about the function, I'll share some of my anguish now. I mistakenly used the carrot operator `^` for the longest time because I was doing many xor operations in Python. That was a bug that took quite some time to figure out. Another place where I messed up was assuming I had correctly solved the entire problem once I just found "107". That's the ascii encoding for "k" which is the end of the 15 byte string that I used to test my program ("paddingoracleattack"). I saw the "k" and thought wow, sweet everything works splendidly. I then proceeded to write the better part of this paper, (only the top half which is still relevant). I figured, "ahh Haskell's slow and there is a lot of file reading and file writing". Well it turns out that the algorithm is actually pretty linear, it will always take time proportional to the length of the ciphertext. So I caved and listened to the little demon lurking in the back of my mind saying "Your program's broken and it's the last day before it's due. Better use that second diploma frame for family photos." Well I denied that annoying little voice and inserted some print statements into my program. This typically involves changing some type signatures to `IO ()` and putting everything in a `do` block. But once I did that I realized that my program wasn't doing what I wanted!

#### More rant, before the oAT explanation

So I saw that I wasn't incrementing the padding byte from right to left in the `attempt_ciphertext.bin` file. Thank you `xxd` tool. So I did the sensible thing and fixed it by writing a new function to change the bytes from right to left. It may seem as though we could have done this from the start, but we need the correct guess for this technique to work. We just recurse over the bytes that we've found plaintext for, and alter them so that the above xor equation works out to the next padding byte. Below is the function that has a little helper which moves up till the index is equal to 16 and then it returns to `oAT`. At one point this was written with a list comprehension, but it was becoming painful to debug so I switched to the recursive function. Also, I only ever needed the last most version of the bytes provided by the list comprehension anyways.


```haskell
-- recursively change all of the previously found characters in expectation of the new find
updateForNextPad     :: [Word8] -> Word8 -> Word8 -> [Word8]
updateForNextPad ws t_posn np   = if t_posn == 16
                                    then ws
                                  else updateForNextPad new_ws (fromIntegral(t_posn + 1)) np
                                  where new_ws = upsert ws (fromIntegral(t_posn)) np

upsert :: [Word8] -> Word8 -> Word8 -> [Word8]
upsert ws posn p   = x ++ val : ys
                     where val = t `xor` (p - 1) `xor` p
                           (x,t:ys) = Prelude.splitAt (fromIntegral(posn)) ws
```

For some reason, I really like the word "upsert" even though it's just an operation used by databases. So the first function in the code block above was mostly described in the preceding text. It has a helper function, `upsert` that changes the data in `ws` until the target position is the length of a block. At which point we pop back to `oAT`. `upsert` is a little more interesting, it's where I spent a fair amount of time figuring out what `val` should be. It seems obvious looking at it now, but the solution required looking at quite a few hexdumps. So we just want to place in the ciphertext the byte that will make the decryption and xoring done in Python equal our padding character `p`. We can see that the `Prelude.splitAt` function required that the prelude was specified because the name clashed with `Data.ByteStrings.Lazy as BL`. Now there is probably a way to `hide` the function from `BL`, but for some reason the code was really complaining here, and not so much at other invokations of `splitAt`. We see here that I used the trick from the StackOverflow article to succinctly insert an element at a particular location in the list. I'm referring to the bottom most line of the above code block. The full list of `Word8` is then returned from this function, only to be called again if we haven't updated all of the ciphertext bytes.

#### Did he just include oAT again?


```haskell
oAT :: [Word8] -> Word8 -> Int -> Word8 -> Word8 -> [Word8] -> IO ()
oAT ws t t_posn g p pt = do let f_part = fst (splitAt t_posn ws)
                            let new_ws = (f_part ++ g:[]) ++ tail (snd (splitAt t_posn ws))
                            createAttemptCiphertext (BL.pack new_ws)
                            ifM isValidCiphertext
                                (do let np = p + 1
                                    let v_new_ws = updateForNextPad new_ws (fromIntegral(t_posn)) 
                                                                           (np)
                                    case p of
                                      16 -> print ( map (chr . fromEnum) (reverse pt))
                                      _  -> oAS v_new_ws (fromIntegral(t_posn - 1))
                                                         (pt ++ ((t `xor` g `xor` p):[])))
                                (oAT new_ws t t_posn (g + 1) p pt)
```

Yes. Yes he did. 

This isn't one of those sweet "operational" Jupyter notebooks, I just like the style. This code has a little too much IO, a little to much complication with the Python callouts, to run as an interpretted notebook. Actually, plain old GHC gave me "ghc: internal error: evacuate(static): strange closure type 46367560". I just run it in GHCi and everything is cheeky.

So again, the type signature for this function is awful. It takes a list of bytes `ws` which is actually the contents of `./binFiles/attempt_ciphertext.bin`. It also takes a target `t`, the target's position `t_posn` in the previous ciphertext's block (relative to the one we're cracking), a guess `g` seeded at `1::Word8`, a padding character `p`, and the currently solved plaintext characters `pt`. Yep, this is a lot to lug around between function calls. But Haskell insisted that I carry my modified state with me, so along it came. Unfortunately I couldn't hack a way to saving the plaintext to a global array like in good ol' C. On the plus side, this may open up parallelization of the hack. Except CBC by it's nature can't support parallelization. So scratch that. 

What do we do on step one? We split the ciphertext bytes at the position indicated by `t_posn` and save the first half in a cleverly named `f_part`. An aside here, sorry if I'm coming off as pompous through the writing. I'm finding it easier to write quickly, I'm pinched for time, and I actually got this to work so I'm pretty proud. Anyways, we take `f_part` and do something very similar to what we've been doing to insert an element `g`. The first two lines just glue together our fragmented list of ciphertext bytes around the guess we're attempting. Again, the guess `g` is just a byte that when `xor`d with the original target `t`, and the `pad` equals the plaintext. But we can't solve for the plaintext, we need to solve for the padding character. See the __isValid?__ section for more detail. 

The third line creates a new ciphertext from which we can discern validity by checking against the server. In short, `createAttemptCiphertext` runs the Python file `./python/try_ciphertext.py`. 

We run the very interesting monadic `ifM` to see if the ciphertext is valid! If the first case evaluates, then we need to increase our padding character. `p` gets one added to it and becomes `np`, short for "new pad". We then call the updateForNextPad with the recently glued together `new_ws` our position cast to the appropriate type, and our new pad `np`. Formatting is a little silly here to get everything to fit on the page, but the final code looks much more neat. We save all of the newly augmented ciphertext bytes in `v_new_ws` short for "very new words". Now we've included our guess `g` and changed all of the ciphertext bytes in anticipation for the incoming round of plaintext cracking. 

From here we fall into the `case` statement. If the padding byte `p` is a 16, congratulations, we've cracked all of the plaintext bytes in this block. If it isn't but we succesfully found a plaintext character, it's time to add that plaintext to the list of known solutions and move on. We'll want to go back up to `oAS` so we can reset the cracking mechanism *i.e.* find the correct target/position/prime starting guess.

If we haven't found the character, we land in the second branch of `ifM`. This being the case, we'll need to increment our guess and try again. Remember that this requires opening a file, writing our guess to a file, running the decryption mechanism, and checking the validity of our ciphertext. Something to note is AES is a symmetric block cipher. That means it's generally a fast algorithm. But the amount of operations taken to crack this actually quite a few.

#### The sum of it all.

That about explains the code, but I'd like to reflect on the undertaking of this programming assignment. First of all, I've listened to well over 32 hours of techno in the last week. A link to my favorite 12 hour mix is in the references (I think this is worth noting [7]). Second of all, this is probably not the correct task for a Haskell project. About midway through I was relatively certain this was impossible. If I hadn't trudged through coding Rust before there is almost certainly no way I would have had the patience to create this program. On the plus side, this is probably my favorite way to learn a language. I'd rather not follow some recipe of "how to create a Haskell program" and put out something all too generic. Instead I find it fun to weed through the metaphorical Amazon, chopping down bushes with the handle of Haskell while holding the blade and complaining it's dull. 

There is a single word from the previous paragraph I'd like to hone in on. *Generic[s]*. Looking through my code you'll see none used by me. Also, you may note that I didn't create any datatypes either. No classes, or instances of those non-existant classes. Nothing at all, and for that reason I feel a little bit like I came up short. Honestly, there isn't even anything particularily beautiful about this program either. It just kind of works. I like to find solice in the fact that it was said in class that "sometimes programs are just yucky looking". That's not a direct quote even though there are quotations around it. This was sometime around showing Java's inner class capability. Anyways, this program came in at a whopping 98 lines of code. But I don't want to sell myself short on this project - it was non-trivial as we like to say. Also, almost all of my testing was done in an interpreter so I can see how it's lacking in that department. But again, this program was mostly about getting the pieces to fit together, so there's really not much to test. Who am I kidding? Of course there's stuff to test. But I know it works, and encourage you to pull it down from GitLab [8] and give it a whirl. Forewarning, there may be a couple of dependencies to download. Below is a list of the imports. There should be README in the repo, but I'll note here that you'll need to make the Python scripts executable by all users. One more thing, this Jupyter notebook is missing the spellcheck plugin, so apologies for the grammatical errors. Now this paragraph was a bit of downer, so we're not going to end here.

Check out that program! Wow, it cracks stuff! How spooky and dangerous. Go into the Python code and change those bytes to something else, I bet this program will figure it out! This was fun, a total ride, and I have absolutely no regrets. However, I can't say I'd like to program this intensively for a little while. Looking forward to spring break. This has been a great class, I hope you also have some time off to enjoy the spring weather.


```haskell
import qualified Data.ByteString.Lazy as BL -- fundamental to the program
import Data.Char (chr)                      -- printing solution 
import Data.Binary.Get                      -- getting file contents
import Data.Bits                            -- `xor` operation
import Data.List                            -- many list functions
import Data.Word                            -- the Word8 type
import System.Process                       -- outsourcing to Python
import Control.Conditional                  -- Mondad ifM
```

#### References 
These are mostly URLs because the authors are typically listed on the webpage or are a click away.

[1] http://hackage.haskell.org/package/pkcs7-1.0.0.1/docs/src/Crypto-Data-PKCS7.html#padBytes

[2] http://hackage.haskell.org/package/binary-0.10.0.0/docs/Data-Binary-Get.html

[3] https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation#Cipher_Block_Chaining_(CBC)

[4] https://stackoverflow.com/questions/5852722/replace-individual-list-elements-in-haskell

[5] https://stackoverflow.com/questions/29717327/monadic-if-how-it-works

[6] http://hackage.haskell.org/package/extra-1.1/docs/src/Control-Monad-Extra.html#ifM

[7] https://www.youtube.com/watch?v=_VfmkgqbZv8&t=40430s

[8] email for link

https://www.youtube.com/watch?v=aH4DENMN_O4

https://web.engr.oregonstate.edu/~rosulekm/crypto/chap9.pdf

https://blog.skullsecurity.org/2013/padding-oracle-attacks-in-depth

https://stackoverflow.com/questions/4702325/best-way-to-convert-between-char-and-word8
