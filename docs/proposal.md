# Project Proposal
## Jeffrey De La Mare
### Cryptopals challenge 17 Padding Oracle Attack

Cryptopals is a culmination of interesting attacks on cryptographic protocols. I started
doing these projects well over a year ago, and have struggled through the first set, part
of the second, and one objective from the fifth. In fact, the fifth was a project that I 
completed for Bryant's Applied Cryptography. For the first set I've shared code with
Bart as it turns out he's also worked on these. I'm mostly interested in completing a 
project from the third set, and was directed to this by a comment in the fourth set. It 
says "... but the attacks themselves involve less code than, say, the CBC padding oracle."
This led me to see exactly what that is, and it's certainly interesting enough. If 
appropriate, I'd like this to be my project.


Based on the problem description, and largely paraphrasing exactly that, the program 
implements two functions:

1) First one submits to the oracle.
   - randomely choose a string `s` from the list of the 10 strings provided.
   - generate an AES key 
   - pad `s` out till it's divisible by block size used in AES encryption (16 bytes)
   - CBC encrypt `s` (could use my own or just OpenSSL's version) with generated AES key 
   - provide a caller with that ciphertext and IV

2) Second openly confirms or denies acceptance of a submission.
   - consume ciphertext and decrypt, determine if padding is valid.
   - return true or false if the padding is valid or invalid respectively

More will be explained about the padding oracle attack once the project is underway / 
completed.

### Timeline
First week: 
* 1 - 2 hours researching the topic. Apparently there are a variety of blog 
  posts on this attack. Assuming none of them contain Haskell code, I feel it's reasonable
  to read about the attack. Perhaps some real world examples.
 
* 7 - 8 hours building out types for this project. What does a submission to the oracle 
  look like? There will almost certainly be IO, how should that be handled?

Second week:
* 4 hours carving out functions. There are two functions outlined by the problem. I'll 
  almost certainly need more for doing the padding, calling into AES, and other 
  transformations.           

* 4 hours that is mostly buffer time to catch up on the estimates above. Dividing the 
  project into modules if need be.

Third week:
* 4 hours for linking the existing code together, making more thorough comments, perhaps
  outlining some test cases.

* 4 hours for outlining and starting the project report.

Fourth week:
* Mostly empty time here to complete testing, project report, and the screencast.

### Stretch goals
If for whatever reason I come up shy of 32 hours worth of work, there is still more to do.
I can implement CBC mode encryption (I've already done this in Rust) by using ECB repeatedly.
I'll have to implement padding, so that isn't a stretch goal but I figured I'd put it here. 
It's possible I'll have a contrived example to get started with, and I'll implement PKCS7 
padding further down the line. Even more of a stretch goal would be to put this in a on a 
web server and try to do a more live-fire type demonstration for the screencast. Although 
these are *stretch* goals.

### Resources

Problem description
http://cryptopals.com/sets/3/challenges/17

Implement CBC Oracle Attack 
https://en.wikipedia.org/wiki/Padding_oracle_attack#Padding_oracle_attack_on_CBC_encryption

Requires implementing padding code
http://cryptopals.com/sets/2/challenges/9

Requires some use of AES package
http://hackage.haskell.org/package/cipher-aes

Youtube vid explanation 
https://www.youtube.com/watch?v=aH4DENMN_O4

